// #pragma once
// #include <stdio.h>
// #include <stdlib.h>
// #include <errno.h>
// #include <string>
// #include <iostream>
// #include "HyperLogLog.h" // for nucToU32

// using namespace std;

// /* An arbitrary initial parameter */
// #define JHASH_INITVAL           0xdeadbeef

// typedef unsigned int u32;

// #define rol32(a,s) a << s

// /* __jhash_final - final mixing of 3 32-bit values (a,b,c) into c */
// #define __jhash_final(a, b, c)                  \
// {                                               \
        // c ^= b; c -= rol32(b, 14);              \
        // a ^= c; a -= rol32(c, 11);              \
        // b ^= a; b -= rol32(a, 25);              \
        // c ^= b; c -= rol32(b, 16);              \
        // a ^= c; a -= rol32(c, 4);               \
        // b ^= a; b -= rol32(a, 14);              \
        // c ^= b; c -= rol32(b, 24);              \
// }

// static uint32_t nucToU32(char* nucleobases) {

	// char* p = nucleobases;
	// uint32_t encoded = 0;

	// for (uint32_t i = 0; i < K; ++i)
	// {
		// //printf("%c\n", (*p));
		// //printf("%d\n", (*p) >> 1 & 3);
		// encoded |= ((*p++) >> 1 & 3) << (K * 2 - 2 * (i + 1));

		// /*
		// --> (*p) >> 1 & 3) does the following:
		// A 65 01000|00|1  0
		// C 67 01000|01|1  1
		// G 71 01000|11|1  3
		// T 84 01010|10|0  2
		// --> (K * 2 - 2 * (i + 1)) does the following:
		// i.e. streamed input is "AGCG" (K=4), you'd want to shift 00(A) value 6 bits left, 11(G) value 4 bits left etc.
		// */
	// }

	// return encoded;
// }

// /* jhash_3words - hash exactly 3, 2 or 1 word(s) */
// static inline void jhash_3words(string a, string b, string c, u32 initval, void* out)
// {
	// u32 a_32 = nucToU32(const_cast<char *>(a.c_str()));
	// u32 b_32 = nucToU32(const_cast<char *>(b.c_str()));
	// u32 c_32 = nucToU32(const_cast<char *>(c.c_str()));

	// a_32 += JHASH_INITVAL;
	// b_32 += JHASH_INITVAL;
	// c_32 += initval;

	// __jhash_final(a_32, b_32, c_32);

	// *(uint32_t*)out = c_32;
// }

// static inline void jhash_3words(u32 a, u32 b, u32 c, u32 initval, void* out)
// {
	// a += JHASH_INITVAL;
	// b += JHASH_INITVAL;
	// c += initval;

	// __jhash_final(a, b, c);

	// *(uint32_t*)out = c;
// }

// static inline void jhash_2words(string a, string b, u32 initval, void* out)
// {
	// u32 a_32 = nucToU32(const_cast<char *>(a.c_str()));
	// u32 b_32 = nucToU32(const_cast<char *>(b.c_str()));

	// jhash_3words(a_32, b_32, 0, initval, out);
// }

// static inline void jhash_2words(u32 a, u32 b, u32 initval, void * out)
// {
	// jhash_3words(a, b, 0, initval, out);
// }

// static inline void jhash_1word(string a, uint32_t len, uint32_t initval, void* out)
// {
	// if(len > 16){
		// cout << "Error: Unable to encode with Jenkins, string length > 16" << endl;
	// }
	// u32 a_32 = nucToU32(const_cast<char *>(a.c_str()));
	// jhash_3words(a_32, 0, 0, initval, out);
// }

// static inline void jhash_1word(u32 a, uint32_t len, u32 initval, void* out)
// {
	// jhash_3words(a, 0, 0, initval, out);
// }

// // void usage()
// // {
	// // fprintf(stderr, "Usage: ./jhash <integer to hash>\n");
	// // fprintf(stderr, "Input integer must fit in 4 bytes (u32).\n");
// // }
