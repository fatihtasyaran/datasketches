#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for getopt

#include "Sketch.h"
#include "HyperLogLog.h"
#include "CountMin.h"
#include <iostream>
#include <string>
#include <fstream>
#include <chrono>

// The following libraries are used for reading genomic datasets from .gz files. Hasn't been implemented yet.
// #include "zlib.h"
// #include "kseq.h"
// KSEQ_INIT(gzFile, gzread)

using namespace std;
/**
* \brief K value redeclared after extern variable in Sketch.h to be used in multiple files.
*/
int K;
/**
* \brief Sketch pointer that points to the user's preferred sketch object.
*/
Sketch* sk;

/**
* \brief Gets and returns the file type for genomic datasets.
* \param in The dataset file as an ifstream object.
* \return File type number.
*/
uint32_t getFiletype(ifstream &in) {
	string hseq;
	getline(in, hseq);
	if (hseq[0] == '>') {
		return 1;
	}

	if (hseq[0] == '@') {
		if ((hseq[1] == 'H'&& hseq[2] == 'D') ||
			(hseq[1] == 'S'&& hseq[2] == 'Q') ||
			(hseq[1] == 'R'&& hseq[2] == 'G') ||
			(hseq[1] == 'P'&& hseq[2] == 'G') ||
			(hseq[1] == 'C'&& hseq[2] == 'O')) {
			return 2;
		}
		else
			return 0;
	}
	return -1;
}
/**
* \brief A parser function for reading and processing Fastq data.
* \param sk The generic class for sketch, is either HyperLogLog or CountMin.
* \param in The dataset file as an ifstream object.
*/
void getEfq(Sketch* sk, ifstream &in) {
	bool good = true;
	for (string seq, hseq; good;) {
		good = static_cast<bool>(getline(in, seq));
		good = static_cast<bool>(getline(in, hseq));
		good = static_cast<bool>(getline(in, hseq));
		if (good && seq.length() >= K) {
			sk->Add(seq);
		}

		good = static_cast<bool>(getline(in, hseq));
	}
}
/**
* \brief A parser function for reading and processing Fasta data.
* \param sk The generic class for sketch, is either HyperLogLog or CountMin.
* \param in The dataset file as an ifstream object.
*/
void getEfa(Sketch* sk, std::ifstream &in) {
	bool good = true;
	int counter = 0;
	for (string seq, hseq; good;) {
		string line;
		good = static_cast<bool>(getline(in, seq));
		while (good&&seq[0] != '>') {
			line += seq;
			good = static_cast<bool>(getline(in, seq));
		}
		if (line.length() >= K) {
			//  cout << counter++ << endl;
			sk->Add(line);
			//hll.Add(line);
			// cout << line << endl;
		}
	}
}
/**
* \brief The function that prints the helper text. Executed when command line options were missing or wrong.
*/
void help() {
	cout << "__ Help __\n-h:\tHash function\t\t(int)\n\t1) CityHash\n\t2) SpookyHash\n\t3) JenkinsHash\n\t4) Murmur3Hash\n\t5) TabulationHash\n\n-l:\tHash bit length\t\t(int)\n\t1) 32\n\t2) 64\n\n-s:\tSketch operation\t(int)\n\t1) Frequency (CountMin)\n\t2) Cardinality (HyperLogLog)\n\n-b:\tBucket bits\t\t\t(int) - Cardinality Only\n(arg > 0 && arg < hashLength)\n\n-k:\tK-mer Size\t\t\t(int)\n(arg > 0)\n\n-f:\tInput file\t\t\t(string)\n(arg is a file path)\n" << endl;
}
/**
The options for arguments are as follows:\n
Hash function: -h\n
Hash length: -l\n
Sketch: -s\n
Bucket bits: -b	(HyperLogLog only)\n
K-mer size: -k\n
Filepath: -f\n

* \param argc Argument Count
* \param argv Argument Values
* \return Success: 0, Failure: -1
* \brief The main function to be executed when the program is ran. 
*/
int main(int argc, char **argv) {
	K = -1;
	// int aflag = 0;
	// int bflag = 0;

	int hValue = -1; //hashfunction
	int lValue = -1; //hashlength
	int sValue = -1; //sketch
	int bValue = -1; //bucketbits
	int kValue = -1; //kmersize
	char* fValue = NULL;

	int hashLength = 0; //32 or 64

	int index;
	int c;

	opterr = 0;

	while ((c = getopt(argc, argv, "h:l:s:b:f:k:v")) != -1) {//hashfunction, hashbits, sketch (bucketBits), filepath (fastq, fasta)
		switch (c)
		{
			// case 'a':
			  // aflag = 1;
			  // break;
			// case 'b':
			  // bflag = 1;
			  // break;
		case 'h':
			hValue = atoi(optarg);
			break;
		case 'l':
			lValue = atoi(optarg);
			break;
		case 's':
			sValue = atoi(optarg);
			break;
		case 'b':
			bValue = atoi(optarg);
			break;
		case 'f':
			fValue = optarg;
			break;
		case 'k':
			kValue = atoi(optarg);
			break;
		case '?':
			if (optopt == 'h' || optopt == 'b' || optopt == 's' || optopt == 'f' || optopt == 'k' || optopt == 'l') {
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
				help();
				return -1;
			}
			else if (isprint(optopt)) {
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				help();
				return -1;
			}

			else {
				fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
				help();
				return -1;
			}

			return 1;
		default:
			help();
			return -1;
		}
	}

	for (index = optind; index < argc; index++)
		printf("Non-option argument %s\n", argv[index]);


	if (sValue > 2 || sValue < 1) {
		cout << "The sketch you entered is invalid" << endl;
		help();
		return -1;
	}

	if (kValue < 1) {
		cout << "The kmer-size you entered is invalid: " << kValue << endl;
		help();
		return -1;
	}


	if (hValue > 5 || hValue < 1) {
		cout << "The hash function you entered is invalid" << endl;
		help();
		return -1;
	}

	if (lValue < 1 || lValue > 2) {
		cout << "The hash bit length you entered is invalid" << endl;
		help();
		return -1;
	}
	K = kValue;
	hashLength = lValue * 32;
	cout << "kmer length: " << K << endl;
	cout << "Hash length: " << hashLength << " bit\n";
	cout << "Sketch: ";

	if (sValue == 1) {
		cout << "CountMin" << endl;
		cout << "hval:" << hValue << "\nlval:" << lValue << endl;
		sk = new CountMin(11, 1023, hValue, hashLength);
	}
	else if (sValue == 2) {
		cout << "HyperLogLog" << endl;
		if (bValue > hashLength - 1 || bValue < 1) {
			cout << "The bucket bit you entered is invalid" << endl;
			help();
			return -1;
		}
		cout << "Bucket bits: " << bValue << " " << endl;
		sk = new HyperLogLog(bValue, hValue, hashLength);
	}

	std::ifstream in(fValue);
	if (!in) {
		cout << "ifstream failed!" << endl;
		help();
		return -1;
	}

	std::string samSeq;
	uint32_t ftype = getFiletype(in);
	cout << "Filetype:" << ftype << endl;
	auto start = std::chrono::system_clock::now();
	if (ftype == 0)
		getEfq(sk, in);
	else if (ftype == 1)
		getEfa(sk, in);
	else {
		cout << "Filetype not recognized." << endl;
		in.close();
		return -1;
	}
	in.close();
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> diff = end - start;
	cout << "File read ended\n";
	cout << "Elapsed time: " << diff.count() << "s" << endl;

	sk->Print();

	if (sValue == 1) {
		string input;
		while (1) {
			cout << "Check the frequency of item (enter q to exit):" << endl;
			cin >> input;
			if (input == "q") {
				break;
			}
			if (input.length() != K) {
				cout << "Invalid K length." << endl;
				continue;
			}
			cout << "Freq: " << sk->Freq(input) << endl;;
		}
	}
	else if (sValue == 2) {
		sk->EstimationEQ();
		cout << endl;
	}

	return 0;
}
