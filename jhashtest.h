#ifndef JHASH_H
#define JHASH_H
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string>
#include <iostream>
//#include "HyperLogLog.h" // for nucToU32

using namespace std;

/* An arbitrary initial parameter */
#define JHASH_INITVAL           0xdeadbeef

typedef unsigned int u32;

#define rol32(a,s) a << s

/* __jhash_final - final mixing of 3 32-bit values (a,b,c) into c */
#define __jhash_final(a, b, c)                  \
{                                               \
        c ^= b; c -= rol32(b, 14);              \
        a ^= c; a -= rol32(c, 11);              \
        b ^= a; b -= rol32(a, 25);              \
        c ^= b; c -= rol32(b, 16);              \
        a ^= c; a -= rol32(c, 4);               \
        b ^= a; b -= rol32(a, 14);              \
        c ^= b; c -= rol32(b, 24);              \
}


class Jhash {
private: 

	int MyArray[255];

public:
	Jhash() {
		MyArray['A'] = 0;
		MyArray['a'] = 0;
		MyArray['C'] = 1;
		MyArray['c'] = 1;
		MyArray['T'] = 2;
		MyArray['t'] = 2;
		MyArray['G'] = 3;
		MyArray['g'] = 3;
		MyArray['N'] = 0;
		MyArray['n'] = 0;
	}

	uint32_t encode(string a) {
		uint32_t encoded=0;
		int length = a.length();
		char* p = NULL;
		p = &a[0];
		for (uint32_t j = 0; j < length; j++) {
			uint32_t x = MyArray[*p];
			encoded = encoded << 2;
			encoded = encoded | x;
			p++;
		}
		return encoded;
	}


	 void jhash_1word(string a, uint32_t len, uint32_t initval, void* out)
	{
		int n = 0;
		int kmer = len;
		string substring="";
		int encoded = 0;
		uint32_t hash = 0;
		while (kmer > n) {
			substring=a.substr(n, 32);
			
			
			encoded=encode(substring);
			uint32_t temp = jhash_3words(encoded, 0, 0, initval, out);
			
			hash^= temp;
			
			n += 32;
		}
		*(uint32_t*)out = hash;
		//cout<<hash<<endl;
	}
	 int jhash_3words(u32 a, u32 b, u32 c, u32 initval, void* out)
	 {
		 a += JHASH_INITVAL;
		 b += JHASH_INITVAL;
		 c += initval;
		
		 __jhash_final(a, b, c);
		
		 
		 return c;
	 }
};





/* jhash_3words - hash exactly 3, 2 or 1 word(s) */


// int jhash_3words(u32 a, u32 b, u32 c, u32 initval, void* out)
// {
	// a += JHASH_INITVAL;
	// b += JHASH_INITVAL;
	// c += initval;

	// __jhash_final(a, b, c);

	// *(uint32_t*)out = c;
	// return c;
// }


// void usage()
// {
// fprintf(stderr, "Usage: ./jhash <integer to hash>\n");
// fprintf(stderr, "Input integer must fit in 4 bytes (u32).\n");
// }
#endif //JHASH_H
