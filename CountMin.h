#ifndef COUNTMIN_H
#define COUNTMIN_H

#define SIGMA 0.0000167017 // 1-sigma will determine the consistency of upper bound, i.e. probability of likelihood of upper bound (will lead to 11 row)
#define EPSILON 0,001//epsilon will determine the upper error bound of the estimation f-real<=f-estimation<(N-setsize)*epsilon+f-real will lead to 200 colm)

#define CM_DEFAULT_ROW 11
#define CM_DEFAULT_COLUMN  200

#include "Sketch.h"

#include <stdint.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include "MurmurHash3.h"
#include "SpookyV2.h"
#include "city.h"
#include "jhashtest.h"

/**
* \brief CountMin class that inherits and implements the virtual Sketch class
*/
class CountMin : public Sketch {
	/** some prime numbers to be used as hash seeds*/
	uint32_t preferred_hash_seeds[250] = { 191 ,193 ,197 ,199 ,211 ,223 ,227 ,229 ,233 ,239 ,241 ,251 ,257 ,263 ,269 ,271 ,277 ,281 ,283 ,293 ,307 ,311 ,313 ,317 ,331 ,337 ,347 ,349 ,353 ,359 ,367 ,373 ,379 ,383 ,389 ,397 ,401 ,409 ,419 ,421 ,431 ,433 ,439 ,443 ,449 ,457 ,461 ,463 ,467 ,479 ,487 ,491 ,499 ,503 ,509 ,521 ,523 ,541 ,547 ,557 ,563 ,569 ,571 ,577 ,587 ,593 ,599 ,601 ,607 ,613 ,617 ,619 ,631 ,641 ,643 ,647 ,653 ,659 ,661 ,673 ,677 ,683 ,691 ,701 ,709 ,719 ,727 ,733 ,739 ,743 ,751 ,757 ,761 ,769 ,773 ,787 ,797 ,809 ,811 ,821 ,823 ,827 ,829 ,839 ,853 ,857 ,859 ,863 ,877 ,881 ,883 ,887 ,907 ,911 ,919 ,929 ,937 ,941 ,947 ,953 ,967 ,971 ,977 ,983 ,991 ,997 ,1009 ,1013 ,1019 ,1021 ,1031 ,1033 ,1039 ,1049 ,1051 ,1061 ,1063 ,1069 ,1087 ,1091 ,1093 ,1097 ,1103 ,1109 ,1117 ,1123 ,1129 ,1151 ,1153 ,1163 ,1171 ,1181 ,1187 ,1193 ,1201 ,1213 ,1217 ,1223 ,1229 ,1231 ,1237 ,1249 ,1259 ,1277 ,1279 ,1283 ,1289 ,1291 ,1297 ,1301 ,1303 ,1307 ,1319 ,1321 ,1327 ,1361 ,1367 ,1373 ,1381 ,1399 ,1409 ,1423 ,1427 ,1429 ,1433 ,1439 ,1447 ,1451 ,1453 ,1459 ,1471 ,1481 ,1483 ,1487 ,1489 ,1493 ,1499 ,1511 ,1523 ,1531 ,1543 ,1549 ,1553 ,1559 ,1567 ,1571 ,1579 ,1583 ,1597 ,1601 ,1607 ,1609 ,1613 ,1619 ,1621 ,1627 ,1637 ,1657 ,1663 ,1667 ,1669 ,1693 ,1697 ,1699 ,1709 ,1721 ,1723 ,1733 ,1741 ,1747 ,1753 ,1759 ,1777 ,1783 ,1787 ,1789 ,1801 ,1811 ,1823 ,1831 ,1847 ,1861 ,1867 ,1871 ,1873 ,1877 ,1879 ,1889 ,1901 ,1907 };

	/** number of buckets, independent of n (number of elements in a set) */
	uint32_t column;

	/** number of different hash functions that map to 1...m, 5 is good for error probability (d) around %1		(k >= ln (1/d))*/
	uint32_t row;

	/** hash table that holds values for each encountered hash value */
	uint32_t** hash_table;

	/** 32 or 64 bit */
	int hashBit;

	/** SpookyHash object, unused if the user prefers another hash method.\n Can be improved. */
	SpookyHash sh;

	/** Function that initializes the newly constructed object. Designed to be run from the CountMin constructor*/
	void init(uint32_t decision, uint32_t hashlength);

public:
	/** \brief Constructor function of the CountMin class.*/
	CountMin(uint32_t _row, uint32_t _column, int decision, int hashlength);
	void Add(std::string param);
	uint32_t Freq(string key);
	/** \brief Function that finds the minimum number in a set of numbers.
		\param values The integer values for occurances of a string in the hash table. Values are retrieved from Freq.
	*/
	int min(uint32_t* values);
	void Print();
};


#endif //COUNTMIN_H
