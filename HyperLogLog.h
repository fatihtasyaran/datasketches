#ifndef HYPERLOGLOGH
#define HYPERLOGLOGH

#include <string>
#include <iostream>
#include <vector>
#include "SpookyV2.h"
#include "Sketch.h"
#include "tabHash.h"
#include "jhashtest.h"

/**
* \brief An arbitrary number to be used by hash functions
*/
#define HASHSEED 17

using namespace std;

/**
* \brief HyperLogLog class that inherits and implements the virtual Sketch class
*/
class HyperLogLog : public Sketch
{
public:
	/**
	* \brief Constructor function of the HyperLogLog class
	\param bitsTaken Assigned to bucketBits
	\param hashDecision Determines the hash method to use
	\param hashLength Assigned to hashBit
	*/
	HyperLogLog(uint32_t bitsTaken, uint32_t hashDecision, uint32_t hashLength);
	void Add(std::string param);
	void Print();
	void EstimationEQ();
	//static void pointerfunc();

private:
	uint32_t numBuckets;
	uint32_t hashSeed = HASHSEED;
	uint32_t* buckets;
	uint32_t bucketBits;
	uint32_t hashBit;
	// unsigned int Encode(char * p);
	uint32_t cardinality;

	SpookyHash sh;
	//Jhash jh;
};


#endif
