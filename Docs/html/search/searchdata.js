var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvwxz",
  1: "_cghjstz",
  2: "cdhjkmstz",
  3: "acefghijmoprstuwz",
  4: "abcdehklmnoprstxz",
  5: "bcgikouvz",
  6: "_abcdefghijklmoprsuz",
  7: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Pages"
};

