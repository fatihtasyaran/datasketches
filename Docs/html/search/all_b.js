var searchData=
[
  ['k',['K',['../tabularhash_8hpp.html#a97d832ae23af4f215e801e37e4f94254',1,'K():&#160;tabularhash.hpp'],['../_sketch_8h.html#a2689c4b3931025b79053532a5f1b0a85',1,'K():&#160;Source.cpp'],['../_source_8cpp.html#a2689c4b3931025b79053532a5f1b0a85',1,'K():&#160;Source.cpp']]],
  ['kroundup32',['kroundup32',['../kseq_8h.html#af808c6880732c15dd7210159c486ff82',1,'kseq.h']]],
  ['ks_5feof',['ks_eof',['../kseq_8h.html#ac14e1f4fdd91e97a90d174f8583eadf5',1,'kseq.h']]],
  ['ks_5frewind',['ks_rewind',['../kseq_8h.html#ab312f9f11a3f941c761610158d593726',1,'kseq.h']]],
  ['ks_5fsep_5fmax',['KS_SEP_MAX',['../kseq_8h.html#a1fce1af0a523ab6af5dafeef42f49f15',1,'kseq.h']]],
  ['ks_5fsep_5fspace',['KS_SEP_SPACE',['../kseq_8h.html#a628051ce3717e8860f6266d9f3510713',1,'kseq.h']]],
  ['ks_5fsep_5ftab',['KS_SEP_TAB',['../kseq_8h.html#ac52f23b1ec5f000c657547c9e8c022c8',1,'kseq.h']]],
  ['kseq_2eh',['kseq.h',['../kseq_8h.html',1,'']]],
  ['kseq_5finit',['KSEQ_INIT',['../kseq_8h.html#a2d9bf933ccc30bf61c558b4e404b246d',1,'kseq.h']]],
  ['kstream_5finit',['KSTREAM_INIT',['../kseq_8h.html#a67c0fb5ce32c154a2c617305af3e6337',1,'kseq.h']]],
  ['kstring_5ft',['KSTRING_T',['../kseq_8h.html#a71b6483c174a21acf9670716ce5dcd06',1,'KSTRING_T():&#160;kseq.h'],['../kseq_8h.html#ac800c6fe2937b30307ab5a457e5bbf19',1,'kstring_t():&#160;kseq.h']]]
];
