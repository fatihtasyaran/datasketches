var searchData=
[
  ['reserved',['reserved',['../structz__stream__s.html#add73791dd19b49c9c68f3f3d328c37db',1,'z_stream_s']]],
  ['rol32',['rol32',['../jhashtest_8h.html#ab45634d348d933fe2387b5210b4c38cf',1,'jhashtest.h']]],
  ['rot64',['Rot64',['../class_spooky_hash.html#a2b3b07af28a0b4f0f920fdb3a31a0c87',1,'SpookyHash']]],
  ['rotl32',['ROTL32',['../_murmur_hash3_8cpp.html#ab32bb365c62dcb0da675e248b7e814a9',1,'ROTL32():&#160;MurmurHash3.cpp'],['../_murmur_hash3_8cpp.html#a1bb665f421e67f5adc7b8acd3cda136e',1,'rotl32(uint32_t x, int8_t r):&#160;MurmurHash3.cpp']]],
  ['rotl64',['ROTL64',['../_murmur_hash3_8cpp.html#a612c70207b96b27f06eff614e3b9ffd9',1,'ROTL64():&#160;MurmurHash3.cpp'],['../_murmur_hash3_8cpp.html#a1a727ab783adafe06deef8bfd6bf314d',1,'rotl64(uint64_t x, int8_t r):&#160;MurmurHash3.cpp']]],
  ['row',['row',['../class_count_min.html#a25b8300091cfa22dfe6bc9550de249c9',1,'CountMin']]],
  ['run_5fhash',['run_hash',['../class_sketch.html#a5923fca51ee0e9a00ee57b0206f1e7b8',1,'Sketch']]]
];
