var searchData=
[
  ['add',['Add',['../class_count_min.html#adeef6ea5ac0f9def7eb28436037c00b7',1,'CountMin::Add()'],['../class_hyper_log_log.html#abbbbe63ae15c00cdca7e1833a71e8569',1,'HyperLogLog::Add()'],['../class_sketch.html#a42e430687398296064b7574d0a10e64e',1,'Sketch::Add()']]],
  ['adler',['adler',['../structz__stream__s.html#ade2217fe31e671be1257731883201223',1,'z_stream_s']]],
  ['allow_5funaligned_5freads',['ALLOW_UNALIGNED_READS',['../_spooky_v2_8cpp.html#a3a68e57d8423fae2c17b76895fc4807d',1,'SpookyV2.cpp']]],
  ['avail_5fin',['avail_in',['../structz__stream__s.html#a0cf177f50dbb49692f27480cbcfde794',1,'z_stream_s']]],
  ['avail_5fout',['avail_out',['../structz__stream__s.html#a45ad2364307af9d944fd39d4eca3ca3c',1,'z_stream_s']]]
];
