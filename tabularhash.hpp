#ifndef H_HASH
#define H_HASH

#include <stdint.h>
#include <random>
#include <iostream>
#include <string>
#define K 20
class TabularHash {
private:
	uint32_t **table;
	table = new uint32_t[K / 4];
	for (int i = 0; i < (K / 4); i++) {
		table[i] = new int 256; 
	}
	  
  static const uint32_t mask = 0x000000ff;
  
public:
  TabularHash() {
    std::random_device r;
    std::default_random_engine eng(r());
    std::uniform_int_distribution<uint32_t> uint_dist;
    for(unsigned i = 0; i < 4; i++) {
      for(unsigned j = 0; j < 256; j++) {
	table[i][j] = uint_dist(eng);
      }
    }
  }

  uint32_t get(uint32_t r, uint32_t c) {
    return table[r][c];
  }
  
  void hash(string data, uint32_t na_len,
	  uint32_t na_seed, void * out) const {
	  for (i = 0; i < K/4; i++) {
		char* p;
		p = data[0];
		 uint8_t encoded;

		  for (uint32_t i = 0; i <4; ++i)
		  {
			  encoded ^= ((*p++) >> 1 & 3) << (8 - 2 * (i + 1));

		  }
		  uint8_t c = encoded;
    uint32_t h = 0;  
      h ^= table[i][c];
      
    }
    h = out;
  }
};
#endif
