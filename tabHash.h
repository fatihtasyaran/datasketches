#ifndef TAB_HASH_H
#define TAB_HASH_H
#include <stdint.h>
#include <random>
#include <iostream>
#include <string>
#include "Sketch.h"

class TabularHash {
private:
	uint32_t **table;
	//uint32_t table[2][255];	
	int MyArray[255];
	
	static const uint32_t mask = 0x000000ff;

public:
	TabularHash() {
	  MyArray['A'] = 0;
	  MyArray['a'] = 0;
	  MyArray['C'] = 1;
	  MyArray['c'] = 1;
	  MyArray['T'] = 2;
	  MyArray['t'] = 2;
	  MyArray['G'] = 3;
	  MyArray['g'] = 3;
	  MyArray['N'] = 0;
	  MyArray['n'] = 0;
	}

	void setK() {
	  table = new uint32_t*[K/4];
	  for (int i = 0; i < (K/4); i++) {
	    table[i] = new uint32_t[256];
	    //	    std::cout<< i;
	  }
	    
	  std::random_device r;
	  std::default_random_engine eng(r());
	  std::uniform_int_distribution<uint32_t> uint_dist;
	  for (unsigned i = 0; i < (K/4); i++) {
	    for (unsigned j = 0; j < 256; j++) {
	      table[i][j] = uint_dist(eng);
	    }
	  }
	}

	
	uint32_t get(uint32_t r, uint32_t c) {
	  return table[r][c];
	}
	
	void hash(std::string data, uint32_t na_len,
		  uint32_t na_seed, void * out) const {
	  
	  uint32_t h = 0;
	  char* p = NULL;
	  p = &data[0];
	  for (int i = 0; i < K / 4; i++) {
	    uint32_t encoded = 0;
	    for (uint32_t j = 0; j < 4; j++) {
	      uint32_t x = MyArray[*p];
	      encoded = encoded << 2;
	      encoded = encoded | x;
	      p++;
	    }
	    
	    //uint32_t encoded=0;
	    h ^= table[i][encoded];
	    
	  }
	  
	  *(uint32_t*)out = h;
	}
};
#endif // TAB_HASH_H
