#ifndef SKETCH_H
#define SKETCH_H

#include <string>

/**
* \brief An extern variable to be defined on runtime and used globally.\n Represents K-mer length.
*/
int extern K;

/**
* \brief A virtual class for types of sketches
*/
class Sketch {
public:
	/**
	\brief Function for adding a strings to the corresponding Sketch.
	\param param A string object parsed from the dataset
	*/
	virtual void Add(std::string param) { };
	/**
	\brief Function for HyperLogLog sketch to make cardinality estimation.
	*/
	virtual void EstimationEQ() { };
	/**
	\brief Function for printing any desired output.
	*/
	virtual void Print() { };
	/**
	\brief Function for CountMin sketch to find the frequency of an item.
	\param key A string object to make a frequency query.
	\return Returns the frequency of the queried item.
	*/
	virtual uint32_t Freq(std::string key) {
		return 1;
	};
/**
*	\brief A function pointer that points to the hash function to be run. Depends on the user's preference of hash method.
	\param param The string to be hashed.
	\param len Hash length parameter
	\param hashSeed hash Seed parameter
	\param hash Hash output
	*/
	void(*run_hash)(std::string param, uint32_t len, uint32_t hashSeed, void *hash);
};

#endif 
