#include "HyperLogLog.h"
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "MurmurHash3.h"
#include <stdlib.h>
#include "SpookyV2.h"
#include "city.h"
#include <stdio.h>
#include <math.h>
#include "jhashtest.h"
#include "tabHash.h"

Jhash temp1;

void Wrapper2(std::string data, uint32_t na_len,
	uint32_t na_seed, void * out) {
	temp1.jhash_1word(data, na_len, na_seed, out);
}

/**
* \brief TabularHash object, unused if the user prefers another hash method.\n Can be improved.
*/
TabularHash ptr;

/**
*	\brief A wrapper function to bypass the member function pointer issue.
	\param data The string to be hashed.
	\param na_len Unused hashlength parameter
	\param na_seed Unused hashseed parameter
	\param out Hash output
*/
void TabularWrapper(std::string data, uint32_t na_len,
	uint32_t na_seed, void * out) {
	ptr.hash(data, na_len, na_seed, out);
}

HyperLogLog::HyperLogLog(uint32_t bitsTaken, uint32_t hashDecision, uint32_t hashLength) {
	hashBit = hashLength;
	bucketBits = bitsTaken;
	uint32_t po = pow(2, bitsTaken);
	buckets = new uint32_t[po];
	cout << " -- " << po << endl;

	numBuckets = po;
	for (int i = 0; i < numBuckets; i++) {
		buckets[i] = 0;
	}
	cardinality = 0;

	uint32_t hash;
	uint64_t hash64;

	if (hashDecision == 5) {
		if (hashLength == 32) {
			ptr.setK();
			run_hash = &TabularWrapper;
		}if (hashLength == 64) {
			cout << "Invalid hashLength for tabular hash" << endl;
		}
	}
	else if (hashDecision == 4) {
		if (hashLength == 32) {
			run_hash = &MurmurHash3_x86_32;
		}
		if (hashLength == 64) {
			run_hash = &MurmurHash3_x64_128;
		}
	}
	else if (hashDecision == 2) {
		if (hashLength == 32) {
			run_hash = &sh.Hash32;
		}
		else if (hashLength == 64) {
			run_hash = &sh.Hash64;
		}
	}
	else if (hashDecision == 1) {
		if (hashLength == 32) {
			run_hash = &CityHash32;
		}
		else if (hashLength == 64) {
			run_hash = &CityHash64;
		}
	}
	else if (hashDecision == 3) {
		if (hashLength == 32) {
			run_hash = &Wrapper2;
			//u32 encode = nucToU32(p, K);
			//hash = jhash_1word(encode, 0x16278182);
		}
		else if (hashLength == 64) {
			cout << "Error: Jenkins Hash in 64 bit is not implemented" << endl;
		}
	}
}

void HyperLogLog::Add(string param) {
	// cout << "HLL_ADD" << endl;

	for (int i = 0; i < param.length() - K + 1; ++i) {
		string substr = param.substr(i, K);
		//cout << "Substring: " << substr << endl;

		unsigned int index = 0;
		unsigned int counter = 0;


		if (hashBit == 32) {
			unsigned int temp = 0;

			uint32_t hash;
			run_hash(substr, K, hashSeed, (void*)&hash);
			index = hash >> (32 - bucketBits);
			//cout<< index<<endl;
			temp = hash << (bucketBits);

			counter = __builtin_clz(temp);
			if (counter > 32 - bucketBits)
			{
				counter = 32 - bucketBits;
			}
			if (buckets[index] < counter + 1) {
				buckets[index] = counter + 1;
			}
			// cout << param << endl;
			// cout << hash << " " << index << " " << counter << " " << buckets[index] << endl;

		}
		else if (hashBit == 64) {
			uint64_t temp64 = 0;

			uint64_t hash64;
			run_hash(substr, K, hashSeed, (void*)&hash64);

			index = hash64 >> (64 - bucketBits);
			temp64 = hash64 << (bucketBits);
			counter = __builtin_clzll(temp64);
			if (counter > 64 - bucketBits)
			{
				counter = 64 - bucketBits;
			}
			if (buckets[index] < counter + 1) {
				buckets[index] = counter + 1;
			}
		}
		/*
		int z = 0;
		int size = (sizeof(unsigned int) * 8);
			  unsigned int temp2 = (1 << 31);
			  while (z<size) {
			  unsigned int temp = (hash << (bucketBits));

			  if ((temp&temp2) == 0) {
			  counter++;
			  }
			  else if ((temp&temp2) != 0) {

			  z = size;

			  }
			  temp2 = (temp2 >> 1);
			  z++;
			  }*/
	}
}

void HyperLogLog::Print() {
	cout << "HyperLogLog Buckets: " << endl;
	int i = 0;
	while (i < numBuckets) {
		cout << buckets[i] << " ";
		i++;
	}
	cout << endl;
}

void HyperLogLog::EstimationEQ() {
	{
		double sum = 0;
		double estimation;
		for (int i = 0; i < numBuckets; i++)
		{
			sum = sum + pow(2, -buckets[i]);
		}

		double alpha;
		switch (numBuckets) {
		case 40:
			alpha = 0.701951;
			break;
		case 32:
			alpha = 0.697123;
			break;
		case 64:
			alpha = 0.70920856;
			break;
		case 48:
			alpha = 0.705174;
		case 56:
			alpha = 0.707479;
		default:
			alpha = 0.7213 / (1.0 + 1.079 / numBuckets);
			break;
		}
		estimation = alpha*numBuckets*numBuckets*(1 / sum);
		cardinality += ceil(estimation);

		cout << "Estimated cardinality: " << cardinality << endl;
	}
}
