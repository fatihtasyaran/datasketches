# Makefile for the GenoSketch program
# outputs an executable file called "genosketch"
# enter "./genosketch" after compiling succesfully

genosketch: Source.cpp Sketch.h CountMin.h CountMin.cpp tabHash.h  HyperLogLog.h  HyperLogLog.cpp MurmurHash3.h city.cpp city.h jhashtest.h MurmurHash3.cpp SpookyV2.cpp SpookyV2.h kseq.h zconf.h zlib.h
	g++ -std=c++11 -o genosketch Source.cpp Sketch.h CountMin.h CountMin.cpp tabHash.h HyperLogLog.h HyperLogLog.cpp MurmurHash3.h city.cpp city.h jhashtest.h MurmurHash3.cpp SpookyV2.cpp SpookyV2.h kseq.h zconf.h zlib.h -lm