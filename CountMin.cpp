#include "CountMin.h"
//#include "..\HashWrapper.h"
#include <limits>

using namespace std;

CountMin::CountMin(uint32_t _row, uint32_t _column, int decision, int hashlength) {
	row = _row; column = _column, hashBit = hashlength;
	init(decision, hashlength);
}

Jhash temp2;

void Wrapper23(std::string data, uint32_t na_len,
	uint32_t na_seed, void * out) {
	temp2.jhash_1word(data, na_len, na_seed, out);
}


void CountMin::init(uint32_t decision, uint32_t hashlength) {
	hash_table = new uint32_t*[row];

	for (uint32_t i = 0; i < row; i++)
	{
		hash_table[i] = new uint32_t[column]{ 0 };
	}
	cout << "row: " << row << endl << "column: " << column << endl;

	uint32_t hash;
	uint64_t hash64;

	if (decision == 4) {
		if (hashlength == 32) {
			run_hash = &MurmurHash3_x86_32; //MurmurHash3_x86_32(p, len, hashSeed, (void*)&hash);
		}if (hashlength == 64) {
			run_hash = &MurmurHash3_x64_128; //MurmurHash3_x64_128(p, len, hashSeed, (void*)&hash64);
		}
	}
	else if (decision == 2) {
		if (hashlength == 32) {
			run_hash = &sh.Hash32; //sh.Hash32(p, len, hashSeed); // returns hash
		}
		else if (hashlength == 64) {
			run_hash = &sh.Hash64; //sh.Hash64(p, len, hashSeed); // returns hash64
		}
	}
	else if (decision == 1) {

		if (hashlength == 32) {
			run_hash = &CityHash32;//CityHash32(p, K); // returns hash
		}
		else if (hashlength == 64) {
			run_hash = &CityHash64; // CityHash64WithSeed(p, K, (uint64_t)hashSeed);// returns hash64

		}
	}
	else if (decision == 3) {

		if (hashlength == 32) {
			run_hash = &Wrapper23;
			//u32 encode = nucToU32(p, K);
			//hash = jhash_1word(encode, 0x16278182);
		}
		else if (hashlength == 64) {
			cout << "Error: Jenkins Hash in 64 bit is not implemented" << endl;
		}
	}
	if (hashlength != 32 && hashlength != 64) cout << hashlength << endl;

}

void CountMin::Add(std::string param) {
	// cout << "CM_ADD" << endl;
	for (int i = 0; i < param.length() - K + 1; ++i) {
		string substr = param.substr(i, K);
		for (uint32_t i = 0; i < row; i++)
		{
			if (hashBit == 32) {
				uint32_t hash;
				run_hash(substr, K, preferred_hash_seeds[i % sizeof(preferred_hash_seeds)], (void*)&hash);
				// cout << "hash%col: "<< hash%column << endl;
				++hash_table[i][hash%column];
			}
			else if (hashBit == 64) {
				uint64_t hash64;
				run_hash(substr, K, preferred_hash_seeds[i % sizeof(preferred_hash_seeds)], (void*)&hash64);
				// cout << "hash64%col: " << hash64%column << endl;
				++hash_table[i][hash64%column];
			}
		}
	}
}

uint32_t CountMin::Freq(string key) {
	uint32_t* values = new uint32_t[row];
	for (uint32_t i = 0; i < row; i++)
	{
		if (hashBit == 32) {
			uint32_t hash;
			run_hash(key, K, preferred_hash_seeds[i % sizeof(preferred_hash_seeds)], (void*)&hash);
			values[i] = hash_table[i][hash%column];
		}
		else if (hashBit == 64) {
			uint64_t hash64;
			run_hash(key, K, preferred_hash_seeds[i % sizeof(preferred_hash_seeds)], (void*)&hash64);
			values[i] = hash_table[i][hash64%column];
		}
	}
	return min(values);
}

void CountMin::Print() {
	cout << "Count Min Hash Table: " << "\n\n";
	for (int i = 0; i < row; ++i) {
		cout << ">Row " << i + 1 << "\n\n";
		for (int j = 0; j < column; ++j) {
			cout << hash_table[i][j] << " ";
		}
		cout << "\n";
	}
	cout << endl;
}

int CountMin::min(uint32_t* values) {
	uint32_t min_number = numeric_limits<uint32_t>::max();
	for (uint32_t i = 0; i < row; ++i)
	{
		if (values[i] < min_number) {
			min_number = values[i];
		}
	}
	return min_number;
}

//CountMin::~CountMin() {
//	for (uint32_t i = 0; i < row; ++i)
//	{
//		delete hash_table[i];
//	}
//
//	delete[] hash_table;
//}
